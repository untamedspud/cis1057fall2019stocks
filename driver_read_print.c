#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int read_in( stock_t stock[ ], const char *infile );
void print_out( const stock_t stock[ ], const int n, const char *title );

const char *input_files[ ] = { "F.csv", "GE.csv",           "MCD.csv",   "UBER.csv", "dpi.csv" };
const char *titles[ ]      = { "Ford",  "General Electric", "McDonalds", "Uber",     "Dow Jones Average", "" };
const int MAXIMUM_BUSINESS_DAYS_IN_YEAR = 260;

int main()
{
	stock_t stock[ MAXIMUM_BUSINESS_DAYS_IN_YEAR ];
	int count;
	int i = 0;

	while ( *titles[ i ] ) {
		count = read_in( stock, input_files[ i ] );
		print_out( stock, count, titles[ i ] );
		i++;
	}
	return EXIT_SUCCESS;
}
