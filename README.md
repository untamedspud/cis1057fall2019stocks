# cis1057fall2019Stocks

Stock market daily close files to be used for input to lab 12.
The files represent 1 year of information about a stock or the Dow Jones
Industrial Average.

There are five files we will use for input to our program.

    F.csv - stock info for Ford Motor Company for 1 year
    GE.csv - stock info for General Electric for 1 year
    MCD.csv - stock info for McDonalds for 1 year
    UBER.csv - stock info for Uber for the amount of time they've been in the
               public market
    dpi.csv - Dow average for each day.
