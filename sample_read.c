#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const int MAX_FIELDS = 10;
const int MAX_FIELD_CHARACTERS = 50;
void process_inline( const char sample_line[], char fields[ MAX_FIELDS ][ MAX_FIELD_CHARACTERS ], int *number_of_fields );
void print_fields( const char fields[ MAX_FIELDS ] [ MAX_FIELD_CHARACTERS ], const int count );


int main()
{
	char sample_line[] = "Date,Open,High,Low,Close,Adj Close,Volume";
	char fields[ MAX_FIELDS ][ MAX_FIELD_CHARACTERS ];
	int number_of_fields;

	FILE *INFILE = fopen( "ge.csv", "r" );
	while ( ! feof( INFILE ) ) {
		char myline[ 200 ];
		fgets( myline, 200, INFILE );
		process_inline( myline, fields, &number_of_fields );
		print_fields( ( const char (*)[ MAX_FIELD_CHARACTERS ] ) fields, number_of_fields );
	}
	fclose( INFILE );

	return EXIT_SUCCESS;
}



/* 
 * Input:   sample_line          - string to be parsed on comma.
 * Output:  fields               - array of strings separated from the sample_line.
 *          number_of_fields     - number of fields parsed.
 * Returns: Nothing.
 * Globals: MAX_FIELDS           - maximum number of fields we'll process.
 *          MAX_FIELD_CHARACTERS - number of characters in a single field.
 * Description:
 *          Use a charcter collection buffer to store characters up to a comma, 
 *          and when the comma is encountered, place an ASCII NUL character in
 *          the buffer and copy it into the output array *fields*.
 *          NOTE that we're doing no error checking here, so if there are any
 *          mistakes in the input data, this function will fail.
 */

void process_inline( const char sample_line[], 
		     char fields[ MAX_FIELDS ][ MAX_FIELD_CHARACTERS ], 
		     int *number_of_fields )
{
	int i;
	char buf[ MAX_FIELD_CHARACTERS ];

	printf( "Processing line %s.\n", sample_line );
	*number_of_fields = 0;


	// initialize collection buffer
	int b;
	b = 0;
	buf[ b ] = '\0';

	// visit each element of the input line
	for ( i = 0; i < strlen( sample_line ); i++ )
		if ( sample_line[ i ] != ',' ) 
			// accumulate the characters in the buffer
			buf[ b++ ] = sample_line[ i ];
		else {
			// make the buffer a string and copy it to the output array
			buf[ b ] = '\0';
			strncpy( fields[ (*number_of_fields)++ ], buf, MAX_FIELD_CHARACTERS );
			b = 0;
			buf[ b ] = '\0';
		}
	// and process the remaining buf
	buf[ b ] = '\0';
	strncpy( fields[ (*number_of_fields)++ ], buf, MAX_FIELD_CHARACTERS );

	return;
}

void print_fields( const char fields[ MAX_FIELDS ] [ MAX_FIELD_CHARACTERS ], const int count )
{
	int i;

	for ( i = 0; i < count; i++ )
		printf( "Field %d = %s.\n", i, fields[ i ] );

	return;
}
